import java.util.Random;

public class Patient extends Person{
    private int health;
    private String disease;
    private int diseasePower;
    private Hospital hospital;
    private Random generator = new Random();
    private Bed bed = null;
    private boolean isOnBed = false;
    public Patient(String name, String lastName, int health,String disease,Hospital hospital) {
        super(name, lastName);
        this.hospital = hospital;
        this.health = health;
        this.disease = disease;
        switch(disease){
            case "Zatrucie": diseasePower = 2;
                break;
            case "Grypa": diseasePower = 3;
                break;
            case "Złamanie ręki": diseasePower = 4;
                break;
            case "Zapalenie płuc": diseasePower = 5;
                break;
            case "Złamanie nogi": diseasePower = 5;
                break;
            case "Malaria": diseasePower = 6;
                break;
        }

        xPosition = 1250;
        yPosition = 150;
    }
    @Override
    public void run() {
        super.run();
        int initialX = xPosition;
        int initialY = yPosition;
        goToRegistration();
        hospital.getRegistration().occupyRegistration();
        move(hospital.getWaitingRoom().getxPosition(),hospital.getWaitingRoom().getyPosition(),5);
        Chair chair = hospital.getWaitingRoom().occupyChair();
        move(chair.getxPosition(),chair.getyPosition(),5);
        bed = hospital.getOperatingRoom().reserveBed();
        hospital.getWaitingRoom().releaseChair(chair);
        move(hospital.getDecontaminationRoom().getxPosition(),hospital.getDecontaminationRoom().getyPosition(),5);
        hospital.getDecontaminationRoom().enter(this);
        move(hospital.getOperatingRoom().getxPosition(),hospital.getOperatingRoom().getyPosition(),5);
        hospital.getDecontaminationRoom().leave();
        move(bed.getxPosition(),bed.getyPosition(),10);
        isOnBed = true;
        hospital.getOperatingRoom().layOnBed(this,bed);
        hospital.getOperatingRoom().releaseBed(bed);
        bed = null;
        if(health > 0) {
            hospital.healedPatientsCounter++;
            disease = "Zdrowy";
            diseasePower = 0;
            move(hospital.getOperatingRoom().getxPosition(), hospital.getOperatingRoom().getyPosition(), 10);
            hospital.getDecontaminationRoom().enter(this);
            move(hospital.getDecontaminationRoom().getxPosition(), hospital.getDecontaminationRoom().getyPosition(), 10);
            hospital.getDecontaminationRoom().leave();
            move(initialX, initialY, 15);
            hospital.getPatients().remove(this);

        }
        else{
            hospital.getPatients().remove(this);
            hospital.deadPatientsCounter++;
        }


    }

    public String toString()
    {
        return super.toString()+ " " + health + " " + disease;
    }
    private  void goToRegistration() {
        while(xPosition != hospital.getRegistration().getxPosition())
        {
            xPosition--;
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(xPosition <= 1050)
            {
                hospital.getRegistration().checkAccesibility();
            }
        }
        while(yPosition != hospital.getRegistration().getyPosition())
        {
            yPosition++;
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            hospital.getRegistration().checkAccesibility();
        }

    }
    public void setHealth(int health) {
        this.health = health;
    }
    public void move(int destX, int destY,int moveDelay){
        while(xPosition != destX)
        {
            if (xPosition > destX)
                xPosition--;
            else
                xPosition++;
            Thread.yield();
            try {
                Thread.sleep(moveDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        while(yPosition != destY)
        {
            if(yPosition > destY)
                yPosition--;
            else
                yPosition++;
            try {
                Thread.sleep(moveDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public int getHealth() {
        return health;
    }
    public Bed getBed(){return bed; }
    public int getDiseasePower(){return diseasePower;}
    public boolean getIsOnBed(){return isOnBed;}
}
