import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BottomPanel extends JPanel implements ActionListener {
    public JLabel text;
    private JButton button;
    private HospitalView view;
    public BottomPanel(HospitalView view) {
        this.view = view;
        button = new JButton("NOWA GRA");
        button.addActionListener(this);
        add(button);
        text = new JLabel(" ");
        add(text);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == button)
            view.newGame();
    }
}






