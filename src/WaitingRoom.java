import java.util.ArrayList;
import java.util.concurrent.Semaphore;

/**
 * Created by kamil on 6/10/17.
 */
public class WaitingRoom {
    private ArrayList<Chair> chairs = new ArrayList<>();
    Semaphore sem = new Semaphore(6);
    private  int xPosition = 830;
    private int yPosition = 50;
    public WaitingRoom() {
        for (int i = 0;i < 6;i++) {
            chairs.add(new Chair((1100-i*100)-20,50));
        }
    }
    public ArrayList<Chair> getChairs() {
        return chairs;
    }
    public int getxPosition() {
        return xPosition;
    }
    public int getyPosition() {
        return yPosition;
    }
    public Chair occupyChair(){
        try {
            sem.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Chair chair = findFreeChair();
        chair.setOccupation(true);
        return chair;
    }
    public Chair findFreeChair(){
        for (Chair chair : getChairs() ) {
            if(!chair.isOccupied()){
                return chair;
            }
    }
    return null;
}
    public void releaseChair(Chair chair){
        chair.setOccupation(false);
        sem.release();
    }
}
