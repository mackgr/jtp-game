import java.util.concurrent.Semaphore;

/**
 * Created by kamil on 6/10/17.
 */
public class Registration {

    private boolean isOccupy = false;
    private  int xPosition =950;
    private  int yPosition=200;

    public int getxPosition() {
        return xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }


    public synchronized void occupyRegistration()
    {
            isOccupy = true;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            isOccupy = false;
            notifyAll();

    }
    public void setOccupy(boolean b)
    {
        isOccupy = b;
    }

    public boolean ifOccupy()
    {
        return isOccupy;
    }
    public synchronized void  checkAccesibility(){
        while(ifOccupy()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
