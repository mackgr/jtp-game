import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kamil on 6/10/17.
 */
public class HospitalView extends JPanel implements MouseListener, KeyListener
{
    ArrayList<Ellipse2D> ovals = new ArrayList<>();

    public BottomPanel panel;
    private BufferedImage map;
    private BufferedImage doctorImage;
    private BufferedImage fog;
    private BufferedImage greenLamp;
    private BufferedImage redLamp;
    private BufferedImage revGreenLamp;
    private BufferedImage revRedLamp;
    private BufferedImage patientImage;
    private Hospital hospital;
    private Ellipse2D oval;

    public HospitalView()
    {
        setSize(1200,600);
        try {
            map = ImageIO.read(this.getClass().getClassLoader().getResource("hospital.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            doctorImage = ImageIO.read(this.getClass().getClassLoader().getResource("doctor.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fog = ImageIO.read(this.getClass().getClassLoader().getResource("mgla.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            greenLamp = ImageIO.read(this.getClass().getClassLoader().getResource("lampaGREEN.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            redLamp = ImageIO.read(this.getClass().getClassLoader().getResource("lampaRED.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            revGreenLamp = ImageIO.read(this.getClass().getClassLoader().getResource("lampaGREENrev.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            revRedLamp = ImageIO.read(this.getClass().getClassLoader().getResource("lampaREDrev.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            patientImage = ImageIO.read(this.getClass().getClassLoader().getResource("pacjent.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        hospital = new Hospital();
        for(int i = 0; i< hospital.getPatients().size(); i++)
        {
            oval = new Ellipse2D.Double(
                    hospital.getPatients().get(i).xPosition,
                    hospital.getPatients().get(i).yPosition,
                    40,
                    40);
            ovals.add(oval);
        }
        this.addMouseListener(this);
        addKeyListener(this);
        this.setFocusable(true);


    }
    public Hospital getHospital() {
        return hospital;
    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(map, 0, 0, null);
        g2.setColor(Color.RED);
        ovals = new ArrayList<>();
        if(hospital.getDeadPatientsCounter() < 10) {
            for (int i = 0; i < hospital.getPatients().size(); i++) {
                oval = new Ellipse2D.Double(
                        hospital.getPatients().get(i).xPosition,
                        hospital.getPatients().get(i).yPosition,
                        40,
                        40);
                g2.drawImage(patientImage, hospital.getPatients().get(i).xPosition, hospital.getPatients().get(i).yPosition, null);
                g.setColor(Color.GREEN);
                g.fillRect(hospital.getPatients().get(i).xPosition, hospital.getPatients().get(i).yPosition - 5, hospital.getPatients().get(i).getHealth() / 2, 5);
                ovals.add(oval);
                //g.setColor(Color.BLACK);
                //g.setFont(new Font("Arial Black", Font.BOLD, 20));
                // g.drawString(Integer.toString(hospital.getPatients().get(i).getHealth()), 10, 25);
                // g2.drawRect (10, 10, 200, 200);


            }
            for (int i = 0; i < hospital.getDoctors().size(); i++) {
                g2.drawImage(doctorImage, hospital.getDoctors().get(i).xPosition, hospital.getDoctors().get(i).yPosition, null);
            }
        }
        else{
            g.setColor(Color.RED);
            g.setFont(new Font("Arial Black", Font.BOLD, 50));
            g.drawString("GAME OVER!", 450, 300);
        }
        if(hospital.getDecontaminationRoom().isOccupied()) {
            g2.drawImage(fog, 0, 0, null);
            g2.drawImage(revRedLamp, 140, 270, null);
            g2.drawImage(redLamp, 270, 60, null);
        }
        else{
            g2.drawImage(revGreenLamp, 140, 270, null);
            g2.drawImage(greenLamp, 270, 60, null);
        }
        g.setColor(Color.CYAN);
        g.fillRect (1083, 540, 110, 50);
        g.setColor(Color.BLUE);
        g.drawRect (1083, 540, 110, 50);
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial Black", Font.BOLD, 15));
        g.drawString("martwi: " + Integer.toString(hospital.getDeadPatientsCounter()), 1090, 555);
        g.drawString("wyleczeni: " + Integer.toString(hospital.getHealedPatientsCounter()), 1090, 580);
    }

    public void mouseClicked(MouseEvent e) {
        for (int i = 0; i < ovals.size(); i++) {
            if( (e.getButton() == 1) && ovals.get(i).contains(e.getX(),e.getY()))
            {
                panel.text.setText(""+hospital.getPatients().get(i).toString());
            }
        }
    }
    @Override
    public void mousePressed(MouseEvent e) {


    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }
    @Override
    public void mouseEntered(MouseEvent e) {


    }

    @Override
    public void mouseExited(MouseEvent e) {


    }
    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        int k = getHospital().getDoctors().get(0).xPosition;
        int l = getHospital().getDoctors().get(0).yPosition;
        switch( keyCode ) {
            case KeyEvent.VK_UP:
                // handle up

                getHospital().getDoctors().get(0).move(k,l - 5,1);
                break;
            case KeyEvent.VK_DOWN:

                getHospital().getDoctors().get(0).move(k,l+5,1);
                // handle down
                break;
            case KeyEvent.VK_LEFT:

                getHospital().getDoctors().get(0).move(k-5,l,1);
                // handle left
                break;
            case KeyEvent.VK_RIGHT :

                getHospital().getDoctors().get(0).move(k+5,l,1);
                // handle left
                // handle right
                break;
            case KeyEvent.VK_SPACE:
                Patient patient = getHospital().getDoctors().get(0).detectPatient();
                getHospital().getDoctors().get(0).healPatient(patient);
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent evt) {

    }

    @Override
    public void keyTyped(KeyEvent evt) {

    }
    public void newGame(){
        if(hospital != null)
            hospital.patientsSpawner.end = true;
        hospital = new Hospital();
        new Thread(hospital.patientsSpawner).start();
    }

}
