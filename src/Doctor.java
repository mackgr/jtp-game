public class Doctor extends Person {
    //String specialization;
    int skill;
    int initialX;
    int initialY;
    Hospital hospital;
    private Bed bed;


    public Doctor(String name, String lastName, int skill, Hospital hospital, int x, int y) {
        super(name, lastName);
        //this.specialization = specialization;
        this.skill = skill;
        this.hospital = hospital;
        xPosition = x;
        yPosition = y;
        initialX = x;
        initialY = y;
    }

    @Override
    public void run() {
        while (true) {

            bed = hospital.getOperatingRoom().waitForPatients(this);
            bed.setDoctor(this);
            //move(xPosition,yPosition - 100,5);
            // move(bed.getxPosition(),bed.getyPosition()- 50,5);
            // healPatient(bed.getPatient());
            bed.setDoctor(null);

        }

    }

    public int getSkill() {
        return skill;
    }

    public void healPatient(Patient patient) {
        if (patient != null && patient.getHealth() < 100 && Math.abs(patient.xPosition - this.xPosition) < 50 && Math.abs(patient.yPosition - this.yPosition) < 50) {
            patient.setHealth(patient.getHealth() + skill);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Patient detectPatient() {
        for (Patient patient : hospital.getPatients()) {
            if (Math.abs(patient.xPosition - this.xPosition) < 50 && Math.abs(patient.yPosition - this.yPosition) < 50)
                return patient;
        }
        return null;
    }

    public Bed getBed() {
        return bed;
    }

    public void move(int destX, int destY, int moveDelay) {
        if(destX > 1200)
            destX = 1200;
        if(destX < 0)
            destX = 0;
        if(destY > 600)
            destY = 600;
        if(destY < 0)
            destY = 0;
        if(!detectCollision(destX,destY)) {
            xPosition = destX;
            yPosition = destY;
        }
        try {
            Thread.sleep(moveDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public boolean detectCollision(int destX, int destY){

        return hospital.getObstacles()[destX][destY];
    }
}