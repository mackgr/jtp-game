import java.util.ArrayList;

public class OperatingRoom {
    private  int xPosition = 200;
    private int yPosition = 250;
    private ArrayList<Bed> beds = new ArrayList<>();
    private int freeBeds = 4;
    public OperatingRoom() {
        for (int i = 0;i < 4;i++) {
            beds.add(new Bed((480-i*150),500));
        }
    }
    public synchronized  Bed reserveBed(){
        while (freeBeds == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Bed bed = findFreeBed();
        bed.setReservation(true);
        freeBeds--;
        return bed;
    }
    public synchronized void layOnBed(Patient patient,Bed bed){
        bed.setPatient(patient);
        notifyAll();
    }
    public int getxPosition() {
        return xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public Bed findFreeBed(){
        for (Bed bed : getBeds() ) {
            if(!bed.isReserved()){
                return bed;
            }
        }
        return null;
    }
    public synchronized void releaseBed(Bed bed){
        while (bed.getPatient().getHealth() < 100 && bed.getPatient().getHealth() > 0 ) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        bed.setReservation(false);
        bed.setPatient(null);
        freeBeds++;
        notifyAll();
    }
    public ArrayList<Bed> getBeds() {
        return beds;
    }
    public synchronized Bed waitForPatients(Doctor doctor){
        while (findBedToTakeCareOf() == null) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Bed bed = findBedToTakeCareOf();
        bed.setDoctor(doctor);
        notifyAll();
        return bed;

    }
    public Bed findBedToTakeCareOf(){
        for ( Bed bed : beds){
            if(bed.getPatient() != null && bed.getDoctor() == null ){
                return bed;
            }
        }
        return null;
    }
}

