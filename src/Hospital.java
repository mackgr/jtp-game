import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.lang.Thread.sleep;

/**
 * Created by kamil on 6/6/17.
 */
public class Hospital
{
    private Random generate = new Random();
    private ArrayList<Patient> patients = new ArrayList<>();
    //private CopyOnWriteArrayList<Patient> patients = new CopyOnWriteArrayList<>();
    private ArrayList<Doctor> doctors = new ArrayList<>();
    private  WaitingRoom waitingRoom = new WaitingRoom();
    public PatientsSpawner patientsSpawner = new PatientsSpawner(this,10);
    private DecontaminationRoom decontaminationRoom = new DecontaminationRoom();
    private OperatingRoom operatingRoom = new OperatingRoom();

    int deadPatientsCounter = 0;
    int healedPatientsCounter = 0;
    private boolean [][] obstacles =  new boolean[1201][601];

    public Hospital() {
        for (int i = 0 ; i < 4; i++)
            addPatient();

        boolean isMale = generate.nextBoolean();
        int skill = 7;
        if(isMale)
            doctors.add(new Doctor(Names.getMaleName(), Names.getMaleSurname(), skill, this,800 ,500));
        else
            doctors.add(new Doctor(Names.getFemaleName(), Names.getFemaleSurname(), skill, this,800 ,500));
        for(Doctor doctor : doctors){
            new Thread(doctor).start();
        }
        drawWalls(0,240,175,240);
        drawWalls(1200,240,240,150);
        drawWalls(240,0,240,85);
        drawWalls(670,240,670,350);
        drawWalls(800,445,670,600);
        drawWalls(990,445,1200,445);
    }
    private Registration registration = new Registration();
    public Registration getRegistration() {
        return registration;
    }
    public WaitingRoom getWaitingRoom() {
        return waitingRoom;
    }
    public void addPatient() {
        boolean isMale = generate.nextBoolean();
        int health = generate.nextInt(80) + 10;
        if(isMale)
            patients.add(new Patient(Names.getMaleName(), Names.getMaleSurname(), health, Names.getDisease(),this));
        else
            patients.add(new Patient(Names.getFemaleName(), Names.getFemaleSurname(), health, Names.getDisease(),this));
    }
    public List<Patient> getPatients()
    {
        return this.patients;
    }
    public DecontaminationRoom getDecontaminationRoom() {
        return decontaminationRoom;
    }
    public OperatingRoom getOperatingRoom() {
        return operatingRoom;
    }
    public ArrayList<Doctor> getDoctors() {
        return doctors;
    }
    public void drawWalls(int startX,int startY,int stopX, int stopY){
        while(startX != stopX)
        {
            obstacles[startX][startY] = true;
            if (startX > stopX)
                startX--;
            else
                startX++;
        }
        while(startY != stopY)
        {
            obstacles[startX][startY] = true;
            if(startY > stopY)
                startY--;
            else
                startY++;
        }


    }
    public boolean[][] getObstacles() {
        return obstacles;
    }
    public int getDeadPatientsCounter() {return deadPatientsCounter;}
    public int getHealedPatientsCounter() {return healedPatientsCounter;}
}
