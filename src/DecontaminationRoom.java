import java.util.concurrent.Semaphore;

public class DecontaminationRoom {
    private  int xPosition = 300;
    private int yPosition = 150;
    private Semaphore sem = new Semaphore(1);
    private boolean isOccupied = false;
    public int getxPosition() {
        return xPosition;
    }
    public int getyPosition() {
        return yPosition;
    }
    public void enter(Person person){
        try {
            sem.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        isOccupied = true;
        person.move(xPosition-100,yPosition,5);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void leave(){
        isOccupied = false;
        sem.release();
    }

    public boolean isOccupied() {
        return isOccupied;
    }
}
