import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

public class MainWindow extends JFrame {
    private HospitalView view;
    private BottomPanel bottomPanel;

    public MainWindow() {
        view = new HospitalView();
        bottomPanel = new BottomPanel(view);
        view.panel = bottomPanel;
        add(bottomPanel, BorderLayout.SOUTH);
        add(view, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setResizable(true);
        setSize(1400, 800);
        Timer timer = new Timer(1, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.repaint();
            }
        });
        timer.start();
        new Thread(view.getHospital().patientsSpawner).start();
        Timer healthTimer = new Timer(700, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                for(Patient p: view.getHospital().getPatients()) {
//                    if(p.getHealth() > 0)
//                        p.setHealth(p.getHealth() - 1);
//                    else
//                        view.getHospital().getPatients().remove(p);
//                }
                for (Iterator<Patient> it = view.getHospital().getPatients().iterator(); it.hasNext(); ) {
                    Patient p = it.next();
                    if(p.getHealth() > 0 && p.getIsOnBed()) {
                        p.setHealth(p.getHealth() - p.getDiseasePower());
                    }

                }



            }
        });
        healthTimer.start();



    }

}