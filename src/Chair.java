/**
 * Created by kamil on 6/10/17.
 */
public class Chair {
   private  int xPosition;
   private int yPosition;
   private boolean isOccupied;

   public Chair(int xPosition,int yPosition){
       isOccupied = false;
       this.xPosition = xPosition;
       this.yPosition = yPosition;
   }

    public int getxPosition() {
        return xPosition;
    }
    public int getyPosition() {
        return yPosition;
    }
    public boolean isOccupied(){
       return isOccupied;
    }
    public void setOccupation(boolean isOccupied){
        this.isOccupied = isOccupied;
    }
}
