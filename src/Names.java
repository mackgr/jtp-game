import java.util.Random;

/**
 * Created by kamil on 6/6/17.
 */
public class Names
{
    private static String[] maleNames = {"Jan","Stanisław","Andrzej","Józef","Tadeusz","Jerzy","Zbigniew","Krzysztof","Henryk","Sławomir",
        "Ryszard","Kazimierz","Marek","Marian","Piotr","Janusz","Władysław","Adam","Wiesław","Zdzisław","Franciszek",
        "Edward","Mieczysław","Roman","Mirosław","Grzegorz","Czesław","Jarosław","Robert","Waldemar","Paweł",
        "Dariusz","Wojciech","Jacek","Eugeniusz","Tomasz","Stefan","Zygmunt","Leszek","Bogdan","Antoni",
        "Mariusz","Włodzimierz","Michał","Zenon","Bogusław","Witold","Aleksander","Bronisław","Wacław","Porfiriusz","Wszebąd"};

    private static String[] femaleNames = {"Maria","Krystyna","Anna","Barbara","Teresa","Elżbieta","Janina","Zofia","Jadwiga","Danuta","Halina","Irena","Ewa",
        "Małgorzata","Helena","Grażyna","Bożena","Stanisława","Jolanta","Marianna","Urszula","Wanda","Alicja","Dorota","Agnieszka",
        "Beata","Katarzyna","Joanna","Wiesława","Renata","Iwona","Genowefa","Kazimiera","Stefania","Hanna","Lucyna","Józefa","Alina",
        "Mirosława","Aleksandra","Władysława","Henryka","Czesława","Lidia","Regina","Monika","Magdalena","Bogumiła","Marta","Marzena","Brunhilda","Rzepicha"};


    private static String[] maleSurnames = {"Nowak","Kowalski","Wiśniewski","Dąbrowski","Lewandowski","Wójcik","Kamiński","Kowalczyk","Zieliński","Szymański",
        "Stonoga","Kozłowski","Jankowski","Wojciechowski","Kwiatkowski","Kaczmarek","Mazur","Krawczyk","Piotrowski","Zabacki","Nowakowski",
        "Pawłowski","Michalski","Nowicki","Adamczyk","Dudek","Zając","Potworek","Jabłoński","Król","Majewski","Olszewski","Jaworski","Wróbel",
        "Malinowski","Kobyszczę","Witkowski","Borczak","Stępień","Górski","Rutkowski","Michalak","Sikora","Ostrowski","Baran","Duda","Szewczyk",
        "Tomaszewski","Pietrzak","Marciniak", "Prostacki","Barbarzyński","Barbarzyńca","Sierściuch","Szatański","Zmroziciel","Bambaryła","Sierściuchowicz","Cyrograf","Kabotyn",
        "Dżdżownica","Brutalny","Binarny","Dziesiętny","Zatwardy","Bezzasadny","Zwartusiał","Zmarkotniał","Zakwiczał","Zgnilizna","Korozja",
        "Reptilianin","Mechatron","Mordziaty","Tarantula","Skorpionek","Anakonda","Zaskroniec","Mordulec","Starywłamywacz",
        "Kłapouchy","Zielonokoński","Maniacki","Cegłomorski","Abacki","Babacki","Cabacki","Dabacki","Fabacki","Gabacki","Jabacki","Trzygacki",
        "Krasnolud","Cukrogórski","Odrdzewiacz","Wyrębywacz","Porowatoruski","Reptiliańczyk","Czterygacki","Labacki","Megazord","Zjedzony","Mumia",
        "Tryglaw","Porcięta","Robactwo","Inaczej","Zbrojownia","Kolubryna","Wyżarty","Podwójny","Zamroczny","Żarłocznik","Żarłacz","Rumcajs",
        "Wieloryb","Siermiężny","Jakktochce","Posępny","Słabosilny","Degrengolada","Utracjusz","Poniekąd","Bynajmniej","Totumfacki","Infinitezymalski",
        "Szachrajstwo","Sofizmat","Łapserdak" };

    private static String[] femaleSurnames = {"Nowak","Kowalska","Wiśniewska","Dąbrowska","Lewandowska","Wójcik","Kamińska","Kowalczyk","Zielińska","Szymańska",
        "Stonoga","Kozłowska","Jankowska","Wojciechowska","Kwiatkowska","Kaczmarek","Mazur","Krawczyk","Piotrowska","Zabacka","Nowakowska",
        "Pawłowska","Michalska","Nowicka","Adamczyk","Dudek","Zając","Potworek","Jabłońska","Król","Majewska","Olszewska","Jaworska","Wróbel",
        "Malinowska","Kobyszczę","Witkowska","Borczak","Stępień","Górska","Rutkowska","Michalak","Sikora","Ostrowska","Baran","Duda","Szewczyk",
        "Tomaszewska","Pietrzak","Marciniak", "Prostacka","Barbarzyńska","Barbarzyńca","Sierściuch","Szatańska","Zmroziciel","Bambaryła","Sierściuchowicz","Cyrograf","Kabotyn",
        "Dżdżownica","Brutalna","Binarna","Dziesiętna","Zatwarda","Bezzasadna","Zwartusiał","Zmarkotniał","Zakwiczał","Zgnilizna","Korozja",
        "Reptilianin","Mechatron","Mordziata","Tarantula","Skorpionek","Anakonda","Zaskroniec","Mordulec","Starywłamywacz",
        "Kłapouchy","Zielonokońska","Maniacka","Cegłomorska","Abacka","Babacka","Cabacka","Dabacka","Fabacka","Gabacka","Jabacka","Trzygacka",
        "Krasnolud","Cukrogórska","Odrdzewiacz","Wyrębywacz","Porowatoruska","Reptiliańczyk","Czterygacka","Labacka","Megazord","Zjedzona","Mumia",
        "Tryglaw","Porcięta","Robactwo","Inaczej","Zbrojownia","Kolubryna","Wyżarta","Podwójna","Zamroczna","Żarłocznik","Żarłacz","Rumcajs",
        "Wieloryb","Siermiężna","Jakktochce","Posępna","Słabosilna","Degrengolada","Utracjusz","Poniekąd","Bynajmniej","Totumfacka","Infinitezymalska",
        "Szachrajstwo","Sofizmat","Łapserdak"};


    private static String[] diseases = {"Złamanie nogi", "Złamanie ręki", "Zapalenie płuc", "Grypa", "Zatrucie", "Malaria"};


    public static String getMaleName()
    {
        int index;
        Random draw = new Random();
        index = draw.nextInt(maleNames.length);


        return maleNames[index];
    }

    public static String getFemaleName()
    {
        int index;
        Random draw = new Random();
        index = draw.nextInt(femaleNames.length);


        return femaleNames[index];
    }

    public static String getMaleSurname()
    {
        int index;
        Random draw = new Random();
        index = draw.nextInt(maleSurnames.length);


        return maleSurnames[index];
    }

    public static String getFemaleSurname()
    {
        int index;
        Random draw = new Random();
        index = draw.nextInt(femaleSurnames.length);


        return femaleSurnames[index];
    }

    public static String getDisease()
    {
        int index;
        Random draw = new Random();
        index = draw.nextInt(diseases.length);


        return diseases[index];


    }


}
